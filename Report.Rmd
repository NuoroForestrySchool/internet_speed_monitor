---
title: "Report"
author: "Roberto Scotti"
date: "10/2/2021"
output: pdf_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
library(tidyverse)
library(jsonlite)
```

# Intro

Having collected data with SpeedTest trough Cron, I can verivy the availability is provede by my provider.

> | `Crontab -l`
> | `> # m h dom mon dow command`
> | `> /15  * * * speedtest –format=json >>st.log`

# Read Internet speed-test logs

```{r}
lgs <- "st.log" %>% 
  fs::path(fs::path_home(), .) %>% 
  file() %>% 
  stream_in()
  
lgs %>% str(1)
```

# Plot Internet availability

From: <https://airwifi.it/domande-frequenti-faq.php>

44\. A quanto corrispondono ping e latenza?

44\. I ping possono variare in base al server interrogato. Una connessione AirWifi presenta un ping di massimo 10ms verso il cuore della rete. Sporadicamente possono verificarsi ping superiori ai 10ms che comunque non compromettono assolutamente la navigazione

```{r}
# > speedtest -h
# > "Machine readable formats (json, ...) use bytes as the unit of measure with max precision

lgs %>% 
  transmute(timestamp = timestamp %>% parse_datetime(locale = locale(tz = "Europe/Rome"))
         , ping_latency_ms = ping$latency
         , dwnl_bwidth_MB = download$bandwidth %>% as.integer()/1e+6
         , upl_bwidth_MB = upload$bandwidth %>% as.integer()/1e+6
         ) %>% 
  pivot_longer(-timestamp, names_to = "metric") %>%
  ggplot(aes(x = timestamp, y = value)) +
  geom_line() + facet_grid(rows = vars(metric), scales = "free_y", )
```
